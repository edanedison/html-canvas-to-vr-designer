let path = require('path');
let webpack = require('webpack');

module.exports = {
    cache: true,
    entry: {
        main: './client/src/index.js'
    },
    output: {
        path: path.join(__dirname, './public/js'),
        filename: 'bundle.js',
        publicPath: './public/js'
    },
    module: {
        noParse: [
          /node_modules\/aframe\/dist\/aframe-master.js/, // for aframe from NPM
          /node_modules\/cannon\/build\/cannon.js/, // for aframe-extras from NPM
          /node_modules\/prebuiltlib\/dist\/build.js/ // for aframe-extras from NPM      
        ],         
        loaders: [{
            test: /\.jsx?$/,
            include: path.join(__dirname, 'client/src'),
            exclude: /node_modules/,
            loader: "babel-loader",
            query: {
                presets: ['es2015', 'react', 'stage-0', 'stage-3']
            }
        }, {
            test: /\.json$/,
            loader: "json-loader"
        }, {
            test: /\.scss$/,
            loaders: ['css-loader', 'sass-loader']
        }, {
            test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
            loader: "file-loader"
        }, {
            test: /\.(jpe?g|png|gif|svg)$/i,
            loaders: [
                'file-loader?hash=sha512&digest=hex&name=[hash].[ext]',
                'image-webpack-loader?bypassOnDebug&optimizationLevel=7&interlaced=false'
            ]
        }]
    }
};
