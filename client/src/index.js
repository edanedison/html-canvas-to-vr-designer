// modules
import 'aframe';
import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import {syncHistoryWithStore} from 'react-router-redux';
import {browserHistory} from 'react-router';

// api
// import FirebaseApi from './api/firebase';

// Store
import initialState from './reducers/initialState';
import configureStore from './store/configureStore'; //eslint-disable-line import/default

// components
import App from './App';

// styles
import './styles/main.scss';

// store initialization
const store = configureStore(initialState);

// Create an enhanced history that syncs navigation events with the store
const history = syncHistoryWithStore(browserHistory, store);
const rootEl = document.getElementById('root');

// Get the state
const getState = () => {
  console.log(store.getState());
};

window.onload = () => {
	ReactDOM.render(
	<Provider store={store}>
		<App history={history} store={store}/>
	</Provider>,
	rootEl
	);
};




// Subscribe to store changes
store.subscribe(getState);

