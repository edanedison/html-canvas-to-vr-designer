import React, { Component } from 'react';
import PropTypes from 'prop-types';
import reactCSS from 'reactcss';

const Connections = ({...props}) => {

  const testProps = {
    'data-test': props.dataTest
  };

  return (
      <div {...testProps}>
        <h4 data-test="count">Connections: <span>{props.count}</span></h4>
      </div>
  );
};

Connections.propTypes = {
  count: PropTypes.number,
  dataTest: PropTypes.string
};


export default Connections;


