import React from 'react';
import test from 'tape';
import { shallow, mount } from 'enzyme';

import Connections from './';

test('that <Connections /> data test attr gets set', (t) => {
  const wrapper = mount(<Connections dataTest="connections" />);
  console.log(wrapper.debug());
  t.equals(wrapper.find('[data-test="count"]').length, 1);
  t.end();
});

test('that <Connections /> receives a value', (t) => {
  const wrapper = mount(<Connections count={1} />);
  console.log(wrapper.debug());
  t.equals(wrapper.find('[data-test="count"] span').node.innerHTML, '1');
  t.end();
});
