import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class HtmlLayout extends Component {
   render () {
    return (
      <html lang='en'>
      <head>
        <meta charSet='utf-8' />
        <meta name='viewport' content='width=device-width, initial-scale=1' />
        <title>Untitled</title>
      </head>
      <body>
        { /* insert the content as a string so that it can be rendered separate with its own checksum for proper server-side rendering */ }
        <div id='root' dangerouslySetInnerHTML={ {__html: this.props.content} } />
        <script src='js/bundle.js'></script>
      </body>
      </html>
    )
  }
}


