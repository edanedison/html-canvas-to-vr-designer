import {Entity} from 'aframe-react';
import React from 'react';

export default props => (
	
<Entity>
	<Entity camera="" look-controls="" position="0 1.8 4" wasd-controls="" {...props}>
		<a-entity cursor="fuse: true; fuseTimeout: 250"
			position="0 0 -1"
			geometry="primitive: ring; radiusInner: 0.02; radiusOuter: 0.03"
			material="color: yellow; shader: flat">
				<a-animation begin="cursor-fusing" easing="ease-in" attribute="scale"
				fill="forwards" from="1 1 1" to="0.1 0.1 0.1"></a-animation>
		</a-entity>
	</Entity>
</Entity>
);