import Sky from 'aframe-gradient-sky';
import React from 'react';

export default props => (
 <a-gradient-sky 
 		material='shader: gradient; topColor: 255 0 0; bottomColor: 0 121 255;'>
 </a-gradient-sky>
 );

