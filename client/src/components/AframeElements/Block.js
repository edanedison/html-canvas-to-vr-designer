import {Entity} from 'aframe-react';
import React from 'react';

export default props => (
 <a-box 
    key={blocks[blockIndex]}
    color={this.state.blockColor} 
    height={this.state.blockHeight}
    width={this.state.blockWidth}             
    position={blocks[blockIndex]} 
    onClick={(e) => this.gazed(blockIndex)}
    animation='dir: alternate; dur: 1000;
                   easing: easeInSine; loop: true; to: #5F5'
    animation__scale='property: scale; dir: alternate; dur: 200;
                   easing: easeInSine; loop: true; to: 1.2 1 1.2'
    animation__yoyo='property: position; dir: alternate; dur: 1000;
                         easing: easeInSine; loop: true; to: 0 2 0'
    />
);