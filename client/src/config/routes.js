import React from 'react';
import {Route, IndexRoute} from 'react-router';
import {canUseDOM} from 'exenv';
import DesignerLayout from '../layouts/Designer';
import DesignerScene from '../scenes/designer';

import WorldLayout from '../layouts/World';
import WorldScene from '../scenes/world';

export function routes() {

  return (
  	<Route>
      <Route path="/input" component={DesignerLayout} >
        <IndexRoute component={DesignerScene} />
      </Route>  
      <Route path="/" component={WorldLayout} >
        <IndexRoute component={WorldScene} />
      </Route>          
     </Route>    
  );
}

export default routes;