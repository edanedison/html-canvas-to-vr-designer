import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Api from '../../api/api';
import interact from 'interactjs';
import {connect} from 'react-redux';
import { Link } from 'react-router';
import {push} from 'react-router-redux';
import { bindActionCreators } from 'redux';
import { updateSessions } from '../../actions/socketActions';

import Connections from '../../components/Connections';

export class Designer extends Component {

    constructor(props) {
        super(props);
        this.api = new Api();
        this.state = {
            id: null,
            connections: 0,
            username: null,
            roomName: '',
            blocks: [],    
            blockCreated: false,
            existingBlocks: []
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleKeyPress = this.handleKeyPress.bind(this);
        this.buildRoom = this.buildRoom.bind(this);
        this.clearHistory = this.clearHistory.bind(this);

    }

    componentDidMount() {

        let existingBlocks = this.state.existingBlocks;
        
        this.api.socket.on('NEW_CONNECTION_STARTED', (data) => {
            // console.log(data)
            
            Object.entries(data.existingBlocks).forEach(
                ([key, value]) => existingBlocks.unshift(value)
            );                                           
            
            this.setState({
                id: data.id,
                connections: data.connections,
                existingBlocks: existingBlocks
            });
            this.updateConnectionCount(data);
        });

        this.api.socket.on('UPDATE_CONNECTION_COUNT', (data) => {
            this.setState({
                connections: data.connections
            });
            this.updateConnectionCount(data);
        });

        this.api.socket.on('HISTORY_CLEARED', (data) => {
            this.setState({
                existingBlocks: []
            });
        });

        this.api.socket.on('BROADCAST_BLOCK', (data) => {
            
            existingBlocks = [] // Remove when not using API
            
            // existingBlocks.unshift(data);
            Object.entries(data.existingBlocks).forEach(
                ([key, value]) => existingBlocks.unshift(value)
            );               
            this.setState({
                existingBlocks: existingBlocks
            });
        });

    }

    updateConnectionCount(data) {
        this.props.actions.updateSessions(data.connections);
    }

    handleChange(event) {
        
        console.log(event.target.id);
        this.setState({ 
            [event.target.id]: event.target.value,
            blockCreated: true
        });
    }

    handleKeyPress(event) {
        if (event.key === 'Enter') {
            this.buildRoom();
        }
    }
    
    initCanvas() {
        
var pixelSize = 10;

interact('.pixel-canvas')
  .origin('self')
  .draggable({
    snap: {
        targets: [ interact.createSnapGrid({
          x: pixelSize, y: pixelSize
        }) ]
    },
    // allow multiple drags on the same element
    maxPerElement: Infinity
  })
  // draw colored squares on move
  .on('dragmove', function (event) {
    var context = event.target.getContext('2d'),
        // calculate the angle of the drag direction
        dragAngle = 0 * Math.atan2(event.dx, event.dy) / Math.PI;

    // set color based on drag angle and speed
    context.fillStyle = 'hsl(' + dragAngle + ', 86%, '
                        + (30 + Math.min(event.speed / 1000, 1) * 50) + '%)';

    // draw squares
    context.fillRect(event.pageX - pixelSize / 2, event.pageY - pixelSize / 2,
                     pixelSize, pixelSize);
    
    // console.log(event.pageX);
    console.log((event.pageX)/10);
    
  })
  // clear the canvas on doubletap
  .on('doubletap', function (event) {
    var context = event.target.getContext('2d');

    context.clearRect(0, 0, context.canvas.width, context.canvas.height);
  });

  function resizeCanvases () {
    [].forEach.call(document.querySelectorAll('.pixel-canvas'), function (canvas) {
      canvas.width = document.body.clientWidth;
      canvas.height = window.innerHeight * 0.7;
    });
  }

  // interact.js can also add DOM event listeners
  interact(document).on('DOMContentLoaded', resizeCanvases);
  interact(window).on('resize', resizeCanvases);        
        
        
    }

    buildRoom() {

      if ( this.state.blockCreated) {

        this.api.socket.emit('SEND_BLOCK', {
            userId: this.state.id,          
            roomName: this.state.roomName,
            depth: this.state.depth,            
            xPos: this.state.xPos,
            yPos: this.state.yPos,
            zPos: this.state.zPos                                    
        });

        this.setState({
            blockCreated: false
        });

      } else {
         alert('Please define a block first!');
      }

    }

    clearHistory() {
        this.api.socket.emit('CLEAR_BLOCK_HISTORY', {
            id: this.state.id
        });
    }

  render() {
        

    const {title} = this.props.route;
        
    console.log(this.state.existingBlocks)
    
    let details = this.state.existingBlocks.map(function (object, i) {
      return (<li key={i}>Id: {object._id}, roomName: {object.roomName} , Height: {object.height}</li>);
    });    
    
    this.initCanvas()

    return (
      <div className="content">
        <Connections count={this.state.connections} />
        <p>Session id: {this.state.id}</p>
        <div className="input-group input-group-lg">
          <input className="form-control" value={this.state.roomName} onChange={this.handleChange} id='roomName' onKeyPress={this.handleKeyPress} placeholder="roomName" />
        </div>       
        <canvas style={{background: '#ccc'}} className="pixel-canvas"></canvas>                            
        <br/>        
        <button className="btn btn-primary" onClick={this.buildRoom}>Update Scene</button>
        <hr/>
       <ul id="details">{details}</ul>
        <button className="btn" onClick={this.clearHistory}>Clear scene</button>
      </div>
    );
  }
}

Designer.propTypes = {
  actions: PropTypes.object,
  dispatch: PropTypes.object,
  route: PropTypes.object
};

function mapStateToProps(ownProps) {
  return {
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({
      updateSessions
    }, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Designer);
