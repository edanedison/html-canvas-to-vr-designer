import React, { Component } from 'react';
import PropTypes from 'prop-types';

class NotFound extends Component {

  render() {

    const {title} = this.props.route;

    return (
      <div className="content">
        <h1>{title}</h1>
        <p>page not found :(</p>
      </div>
    );
  }
}

NotFound.propTypes = {
  route: PropTypes.object
};


export default NotFound;
