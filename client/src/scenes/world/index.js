import aframe from 'aframe';
import {Entity} from 'aframe-react';
import React from 'react';
import PropTypes from 'prop-types';
import Api from '../../api/api';
import { updateSessions } from '../../actions/socketActions';

import {connect} from 'react-redux';
import { Link } from 'react-router';
import {bindActionCreators} from 'redux';


// import 'aframe-animation-component';
import registerClickDrag from 'aframe-click-drag-component';

import Camera from '../../components/AframeElements/Camera';
// import Sky from '../../components/AframeElements/Sky';
// import Floor from '../../components/AframeElements/Floor';
// import Text from '../../components/AframeElements/Text';

import images from '../../config/images';

registerClickDrag(aframe);

class World extends React.Component {

  constructor(props) {
      super(props);
      this.api = new Api();
      this.state = {          
          cameraPosition: '0 0 5',
          cameraRotation: '0 0 0',
          existingBlocks: []
      };

      // this.getRandomColor = this.getRandomColor.bind(this);
  }

  
 
  
  componentDidMount() {  
    
            let existingBlocks = [];    
  
          this.api.socket.on('NEW_CONNECTION_STARTED', (data) => {
            // console.log(data)
            
            existingBlocks = [];
            
            Object.entries(data.existingBlocks).forEach(
                ([key, value]) => existingBlocks.unshift(value)
            );                                           
            
            this.setState({
                existingBlocks: existingBlocks
            });
            
            console.log(this.state.existingBlocks[0].width);

        });   
        
         this.api.socket.on('BROADCAST_BLOCK', (data) => {
            
            existingBlocks = [] // Remove when not using API
            
            // existingBlocks.unshift(data);
            Object.entries(data.existingBlocks).forEach(
                ([key, value]) => existingBlocks.unshift(value)
            );               
            this.setState({
                existingBlocks: existingBlocks
            });
        });          
  
  }
  
  
  // gazed(blockIndex) {
  //     return this.setState({
  //         blockColor: this.getRandomColor()
  //     });
  // }
  
  // getRandomColor() {
  //     let letters = '0123456789ABCDEF';
  //     let color = '#';
  //     for (let i = 0; i < 6; i++ ) {
  //         color += letters[Math.floor(Math.random() * 16)];
  //     }
  //     return color;
  // }  
  
    
  getRandomArbitrary(min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
  }
  
  render() {
    
    console.log(this.state.existingBlocks)    
    console.log(navigator.getGamepads())
    
    let _self = this;
    
    let blocks = this.state.existingBlocks.map(function (object, i) {
    
    let position = object.xPos + ' ' + (object.yPos + (object.height / 2))  + ' ' + object.zPos;

      return (
            <a-box 
            key={i}
            click-drag
            random-color
            width={object.width}   
            height={object.height}  
            depth={object.depth}                      
            position={position}
            />

        );
    });   
   


    return (
      <Entity>      
        
        <a-entity rotation={this.state.cameraRotation} position={this.state.cameraPosition} camera="userHeight: 1.6" look-controls wasd-controls></a-entity>
        
        {/*<Camera rotation={this.state.cameraRotation} position={this.state.cameraPosition} /> */}
       
        <a-sky id="background" src="#skyTexture" theta-length="90" radius="30"></a-sky>
        <a-cylinder id="ground" src="#groundTexture" radius="32" height="0.1"></a-cylinder>

<a-entity mixin="voxel" click-drag position="-1 0 -2"></a-entity>
  <a-entity mixin="voxel" click-drag position="0 0 -2"></a-entity>
  <a-entity mixin="voxel" click-drag position="0 1 -2">
    <a-animation attribute="rotation" to="0 360 0" repeat="indefinite"></a-animation>
  </a-entity>
  <a-entity mixin="voxel" click-drag position="1 0 -2"></a-entity>
  
  {blocks}
 

      </Entity>
    );
  }
}

World.propTypes = {
  actions: PropTypes.object
};

function mapStateToProps(state, ownProps) {
  return {
  };
}


function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({
      updateSessions
    }, dispatch)
  };
}


export default connect(mapStateToProps, mapDispatchToProps)(World);
