import * as types from '../actions/actionTypes';
// import {push} from 'react-router-redux';
import initialState from './initialState';

export default function socketReducer(state = initialState.socket, action) {
  switch (action.type) {
    case types.UPDATE_SESSIONS_SUCCESS:
      return Object.assign({}, state, {
        sessions: action.value
      });
    default:
      return state;
  }
}
