import {combineReducers} from 'redux';

import socket from './socketReducer';

import { routerReducer } from 'react-router-redux';


const rootReducer = combineReducers({
  routing: routerReducer,
  socket
});

export default rootReducer;
