import React from 'react';
import aframe from 'aframe';
import {Entity, Scene} from 'aframe-react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import reactCSS from 'reactcss';
import 'aframe-auto-detect-controllers-component';

aframe.registerComponent('random-color', {
  dependencies: ['material'],

  init: function () {
    // Set material component's color property to a random color.
    this.el.setAttribute('material', 'color', getRandomColor());
  }
});

function getRandomColor() {
  const letters = '0123456789ABCDEF';
  let color = '#';
  for (let i = 0; i < 6; i++ ) {
    color += letters[Math.floor(Math.random() * 16)];
  }
  return color;
}

aframe.registerComponent('snap', {
  dependencies: ['position'],

  schema: {
    offset: {type: 'vec3'},
    snap: {type: 'vec3'}
  },

  init: function () {
    this.originalPos = this.el.getAttribute('position');
  },

  update: function () {
    const data = this.data;

    const pos = aframe.utils.clone(this.originalPos);
    pos.x = Math.floor(pos.x / data.snap.x) * data.snap.x + data.offset.x;
    pos.y = Math.floor(pos.y / data.snap.y) * data.snap.y + data.offset.y;
    pos.z = Math.floor(pos.z / data.snap.z) * data.snap.z + data.offset.z;

    this.el.setAttribute('position', pos);
  }
});

class WorldLayout extends React.Component {

  constructor(props, context) {
    super(props, context);
    this.state = {
      isLoaded: false
    };
  }

  render() {

    const {actions} = this.props;
    return (
      <div style={{position: 'absolute', height: '100%', width: '100%'}}>
      <Scene> 
      
      <a-entity auto-detect-controllers></a-entity>

        <a-assets>
          <img id="groundTexture" src="https://cdn.aframe.io/a-painter/images/floor.jpg" />
          <img id="skyTexture" src="https://cdn.aframe.io/a-painter/images/sky.jpg" />

          <a-mixin id="voxel"
             geometry="primitive: box; height: 0.5; width: 0.5; depth: 0.5"
             material="shader: standard"
             random-color
             snap="offset: 0.25 0.25 0.25; snap: 0.5 0.5 0.5">
          </a-mixin>

        </a-assets>  
              
        {this.props.children}
      </Scene>
      </div>
      );
  }
}

WorldLayout.propTypes = {
  children: React.PropTypes.object,
  actions: React.PropTypes.object
};

function mapStateToProps(state, ownProps) {
  return {
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({
      // signOut
    }, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(WorldLayout);
