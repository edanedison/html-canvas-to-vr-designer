import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import settings from '../config/settings';
import reactCSS from 'reactcss';


const styles = reactCSS({
  'default': {
    container: {
      background: '#f8f8f8',
      padding: '1rem'
    },
    title: {
      color: '#a30303'
    }
  }
});


class Designer extends Component {

  constructor(props, context) {
    super(props, context);
    this.state = {
      spinner: false
    };
  }

  onLoading() {
    return this.setState({
      spinner: true
    });
  }

  render() {

    return (
      <div style={styles.container}>
        <h1 style={styles.title}>{settings.title}</h1>
        <hr/>
        {this.props.children}
      </div>
      );
  }
}

Designer.propTypes = {
  children: PropTypes.object,
  loading: PropTypes.bool
};

function mapStateToProps(state, ownProps) {
  return {
    loading: state.ajaxCallsInProgress > 0
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({
      // signOut
    }, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Designer);
