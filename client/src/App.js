import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {Router} from 'react-router';

import routes from './config/routes';

class App extends Component {

  render() {
    const { history, store } = this.props;
    return (
      <Router routes={routes(store)} history={history} />
    );
  }
}

App.propTypes = {
  history: PropTypes.object.isRequired,
  store: PropTypes.object.isRequired
};

export default App;
