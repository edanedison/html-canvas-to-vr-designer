export const SET_MESSAGES = 'SET_MESSAGES';

export function fetchMessages() {
    return dispatch => {
        return fetch('/api/messages').then(function(response) {
            if (response.ok) {
                response.json().then(function(response) {
                    if(response.data.length > 0) {
                        dispatch({
                            type: SET_MESSAGES,
                            tweets: response.data
                        });
                    }
                });
            } else {
                console.log("Looks like the response wasn't perfect, got status", response.status);
            }
        }, function(e) {
            console.log("Fetch failed!", e);
        });
    };
}

// export function postMessage(message) {
//     console.log(message);

//     // const token = localStorage.getItem('token');

//     return dispatch => {
//         return fetch('/message', {
//             method: 'post',

//             body: JSON.stringify(message),

//             headers: {
//                 'Content-Type': 'application/json'
//             }
//         })
//             .then(response => response.json())
//     }
// }

