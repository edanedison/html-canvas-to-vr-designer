import {push} from 'react-router-redux';
import * as types from './actionTypes';

function extractSocketProperties() {

  const socket = {};
  const socketProperties = [
    'updateSessions'
  ];
}

export function updateSessions(connections) {
  return {
    type: types.UPDATE_SESSIONS_SUCCESS,
    value: connections
  };
}






