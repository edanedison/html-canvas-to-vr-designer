import mongoose, { Schema } from 'mongoose';

// Define movie schema
const MessageSchema = new Schema({
    id: String,
    created: Date,
    message: String
});

let Message = mongoose.model('messages', MessageSchema);

export default Message;