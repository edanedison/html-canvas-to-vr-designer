import mongoose, { Schema } from 'mongoose';

// Define movie schema
const BlockSchema = new Schema({
    blockId: String,
    width: Number,
    height: Number, 
    depth: Number,      
    color: String,
    xPos: Number,
    yPos: Number,
    zPos: Number,
    rotation: Number,
    scale: Number
});

let Block = mongoose.model('blocks', BlockSchema);

export default Block;