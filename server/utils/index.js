import Block from '../models/block';

const request = require('request');


var self = module.exports = {

    listenForBlock: function(socket, io) {
        socket.on('SEND_BLOCK', (data) => {
            console.log(data.userId + ' created "' + data.blockArray + '"');
            self.saveBlock(data);
            self.broadcastBlock(data, io, request);
        });
    },

    saveBlock: function(data) {

        var block = new Block({
            userId: data.userId,
            // blockId: data.blockId,
            width: data.width,
            height: data.height,
            depth: data.depth,            
            xPos: data.xPos,
            yPos: data.yPos,                        
            zPos: data.zPos,            
            created: new Date()
        });

        block.save(function(err, msg) {
            console.log("Block saved");
        });
    },

    broadcastBlock: function(data, io) {

        // io.emit('BROADCAST_BLOCK', {
        //     userId: data.userId,
        //     width: data.width,
        //     height: data.height,
        // });        
        
        let existingBlocks = [],
            url = 'http://localhost:8080/api/blocks';

        request({
            url: url,
            json: true
        }, (error, response, json) => {

            if (!error && response.statusCode === 200) {

                io.emit('BROADCAST_BLOCK', {
                    existingBlocks: json.data
                });

            } else {
              console.log("Error connecting to API")
            }
        })        
        
    },


    clearBlockHistory: function(socket, io, mongoose) {

        socket.on('CLEAR_BLOCK_HISTORY', (data) => {
          console.log(data.userId + " cleared the scene history");
          mongoose.connection.db.collection('blocks').remove({})
          io.emit('HISTORY_CLEARED', {
              userId: data.userId
          });
        });
    },

    getStoredBlocks: function(socket, io, request, port, connections) {

        let existingBlocks = [],
            url = 'http://localhost:8080/api/blocks';

        request({
            url: url,
            json: true
        }, (error, response, json) => {

            if (!error && response.statusCode === 200) {

              console.log(json.data)

                // Object.entries(json.data).forEach(
                //     ([key, value]) => existingBlocks.unshift(`${ value._id, value.width, value.height }`)
                // );

                // console.log(existingBlocks);

                socket.emit('NEW_CONNECTION_STARTED', {
                    id: socket.id,
                    connections: connections,
                    existingBlocks: json.data
                });

                console.log(connections + ' connections')

                io.emit('UPDATE_CONNECTION_COUNT', {
                    connections: connections
                });

            } else {
              console.log("Error connecting to API")
            }
        })
    }
}
