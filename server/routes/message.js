// Imports
import express, { Router } from 'express';
import lodash, { isEmpty } from 'lodash';

import authCheck from './middleware/auth';
import Message from '../models/message';

const messageRoutes = Router();

messageRoutes.get('/messages', (request, response) => {
    let responseData = {
        success: false,
        data: {},
        errors: []
    };


	  Message.find().lean().exec(function(error, documents) {

        if(documents.length > 0) {
            responseData.data = documents.map(message => ({...message}));
            responseData.success = true;
        }

	    response.json(responseData);

    });

});


export default messageRoutes;
