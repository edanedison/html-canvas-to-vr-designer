'use strict';

const jwt = require('express-jwt');

const auth_secret = 'pukQ5_O_9aX5_EUoUWMYQkv43JBdrHzzQVhq2ooAdVsqqYutC_aapmM3U1W';
const auth_client = 'yVp0xFWs6QNtnB0opoQ44OYCSYfoOWxC';

let authCheck = jwt({
    secret: auth_secret,
    audience: auth_client
});

module.exports = authCheck;