// Imports
import express, { Router } from 'express';
import lodash, { isEmpty } from 'lodash';

import authCheck from './middleware/auth';

const worldRoutes = Router();
  
  //get auth credentials from server
  worldRoutes.get('/world', function(req, res) {
    res.json(req);
  });  

export default worldRoutes;



