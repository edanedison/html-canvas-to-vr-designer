// Imports
import express, { Router } from 'express';
import lodash, { isEmpty } from 'lodash';

import authCheck from './middleware/auth';
import Block from '../models/block';

const blockRoutes = Router();

blockRoutes.get('/blocks', (request, response) => {
    let responseData = {
        success: false,
        data: {},
        errors: []
    };


	  Block.find().lean().exec(function(error, documents) {

        if(documents.length > 0) {
            responseData.data = documents.map(block => ({...block}));
            responseData.success = true;
        }

	    response.json(responseData);

    });

});


export default blockRoutes;
