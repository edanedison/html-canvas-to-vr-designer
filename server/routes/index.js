// Imports
import express, { Router } from 'express';

const authCheck = require('./middleware/auth');

// Common Routes
const commonRoutes = Router();

// Root
commonRoutes.get('/', (request, response) => {
    let responseData = {
        success: false,
        errors: {}
    };

    response.json(responseData);
});

export default commonRoutes;