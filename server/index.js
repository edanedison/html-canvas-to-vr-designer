// Require packages

const path = require('path'),
      express = require("express"),
      app = express(),
      http = require('http'),
      mongoose = require('mongoose'),
      morgan = require('morgan'),
      jwt = require('express-jwt'),
      bodyParser = require('body-parser'),
      cors = require('cors'),
      request = require('request'),
      expressMongoRest = require('express-mongo-rest'),
      fallback = require('express-history-api-fallback'),
      cookieParser = require('cookie-parser'),
      socketio = require('socket.io'),
      server = http.Server(app),
      io = socketio(server);

import React from 'react';
import ReactDOM from 'react-dom/server';
import { renderToString, renderToStaticMarkup } from 'react-dom/server';
import { match, RouterContext } from 'react-router';

// Import API routes

import commonRoutes from './routes';
import messageRoutes from './routes/message';
import blockRoutes from './routes/block';

import utils from './utils';
import config from './config';

// Configure app

const auth_secret = 'pukQ5_O_9aX5_EUoUWMYQkv43JBdrHzzQVhq2ooAdVsqqYutC_aapmM3U1W';
const auth_client = 'yVp0xFWs6QNtnB0opoQ44OYCSYfoOWxC';
// const root = path.join(__dirname, '../public');

let authCheck = jwt({
    secret: auth_secret,
    audience: auth_client
});

// Connect to mongo database

mongoose.Promise = Promise;
mongoose.connect(config.database);

// Setup

app.set('APP_SECRET', config.secret);

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
// app.use(morgan('combined'));
app.use(cors());
// app.use(express.static(root));

// Cookie Parser
app.use(cookieParser());

// Routes

app.use('/input', express.static(path.join(__dirname, '../public')));
app.use('/', express.static(path.join(__dirname, '../public')));

app.use('/api', commonRoutes);
app.use('/api', messageRoutes);
app.use('/api', blockRoutes);




// Initiate Socket

let connections = 0;

io.on('connection', (socket) => {

    console.log('New connection: ' + socket.id);

    connections++;

    // Handle messaging actions

    utils.getStoredBlocks(socket, io, request, config.port, connections);
    utils.listenForBlock(socket, io);
    utils.clearBlockHistory(socket, io, mongoose);

    socket.on('disconnect', () => {
        connections--;
        console.log(socket.id + ' disconnected');
        io.emit('UPDATE_CONNECTION_COUNT', {
              connections: connections
        });
    });

});


// Start the server

server.listen(config.port, () => {
    console.log('Listening on port: ' + config.port);
});



// Export
module.exports = app;

